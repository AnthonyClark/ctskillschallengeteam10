<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="import" href="navbar.html">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/login.js"></script>
</head>
<body >

	<div class="container">
		<div class="page-header">
			<h2 align="center">Query</h2>
		</div>
		<div class="jumbotron col-sm-offset-2 col-sm-8 ">
			<form class="form-horizontal" role="form" method="post"
				action="get_data">
				<div class="form-group">
					<label class="control-label col-sm-2" for="query_st">Query</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="query_st" required>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-6">
						<input type="submit" class="btn btn-success btn-md col-sm-5"> 
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>