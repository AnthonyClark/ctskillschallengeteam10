package com.utillities;
import com.wolfram.alpha.WAEngine;
import com.wolfram.alpha.WAException;
import com.wolfram.alpha.WAPlainText;
import com.wolfram.alpha.WAPod;
import com.wolfram.alpha.WAQuery;
import com.wolfram.alpha.WAQueryResult;
import com.wolfram.alpha.WASubpod;

public class DataRequest {

public String url ="http://api.wolframalpha.com/v2/query";
String query="";
private static String appid="VR5524-2HL8E7VAW8";
WAEngine engine;

public DataRequest() {
engine	= new WAEngine();
engine.setAppID(appid);

}

public String getquery(String qs)
{
	 String result_str = "";
	 engine.addFormat("plaintext");

     // Create the query.
     WAQuery query = engine.createQuery();
     
     // Set properties of the query.
     query.setInput(qs);
     
     try {
         // For educational purposes, print out the URL we are about to send:
         result_str +="Query URL:";
         result_str =engine.toURL(query);
         result_str +="";
         
         // This sends the URL to the Wolfram|Alpha server, gets the XML result
         // and parses it into an object hierarchy held by the WAQueryResult object.
         WAQueryResult queryResult = engine.performQuery(query);
         
         if (queryResult.isError()) {
             result_str +="Query error";
             result_str +="  error code: " + queryResult.getErrorCode();
             result_str +="  error message: " + queryResult.getErrorMessage();
         } else if (!queryResult.isSuccess()) {
             result_str +="Query was not understood; no results available.";
         } else {
             // Got a result.
             result_str +="Successful query. Pods follow:\n";
             for (WAPod pod : queryResult.getPods()) {
                 if (!pod.isError()) {
                     result_str =pod.getTitle();
                     result_str +="------------";
                     for (WASubpod subpod : pod.getSubpods()) {
                         for (Object element : subpod.getContents()) {
                             if (element instanceof WAPlainText) {
                                 result_str +=((WAPlainText) element).getText();
                                 result_str +="";
                             }
                         }
                     }
                     result_str +="";
                 }
             }
             // We ignored many other types of Wolfram|Alpha output, such as warnings, assumptions, etc.
             // These can be obtained by methods of WAQueryResult or objects deeper in the hierarchy.
         }
     } catch (WAException e) {
         e.printStackTrace();
     }	
return result_str;
}

	
}
