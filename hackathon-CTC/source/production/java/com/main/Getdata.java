package com.main;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utillities.DataRequest;
import com.utillities.Utill;


public class Getdata extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	DataRequest dr;
    /**
     * @see HttpServlet#HttpServlet()
     */
  @Override
public void init() throws ServletException {
	// TODO Auto-generated method stub
	   dr = new DataRequest();
  }	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String qu=request.getParameter("query_st");
		
		out.println(Utill.get_head());
		out.println("<p>"+dr.getquery(qu)+"</p>");
		out.println(Utill.get_tail());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String qu=request.getParameter("query_st");
		
		out.println(Utill.get_head());
		out.println("<p>"+dr.getquery(qu)+"</p>");
		out.println(Utill.get_tail());
	}

}
